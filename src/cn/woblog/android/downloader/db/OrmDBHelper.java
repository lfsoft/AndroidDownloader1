package cn.woblog.android.downloader.db;

import java.sql.SQLException;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import cn.woblog.android.downloader.domain.DownloadInfo;
import cn.woblog.android.downloader.utils.MyLog;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class OrmDBHelper extends OrmLiteSqliteOpenHelper {

	// public static void setDBName(int uid) {
	// OrmDBHelper.uid = uid;
	// DB_NAME = "downloaderdb_" + uid;
	// }

	public static int uid;
	public static String DB_NAME = "downloaderdb.db";
	public static final int DB_VERSION = 1;

	public OrmDBHelper(Context context, String databaseName,
			CursorFactory factory, int databaseVersion) {
		super(context, DB_NAME, factory, DB_VERSION);
	}

	public OrmDBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		MyLog.d("create dbname:" + DB_NAME);
	}

	@Override
	public void onCreate(SQLiteDatabase database,
			ConnectionSource connectionSource) {
		try {
			TableUtils.createTable(connectionSource, DownloadInfo.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase database,
			ConnectionSource connectionSource, int arg2, int arg3) {
	}

}
