package cn.woblog.android.downloader.db;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import cn.woblog.android.downloader.domain.DownloadInfo;
import cn.woblog.android.downloader.domain.DownloadStatus;
import cn.woblog.android.downloader.utils.MyLog;

import com.j256.ormlite.dao.Dao;

public class DBController {
	private static final String SQL_DELETE_ALL = "delete from DownloadInfo";
	private static DBController instance;
	private OrmDBHelper mDBhelper;

	private DBController(Context context) {
		mDBhelper = new OrmDBHelper(context.getApplicationContext());
	}

	public static DBController getInstance(Context context) {
		if (instance == null) {
			instance = new DBController(context);
		}
		return instance;
	}

	public void newOrUpdate(DownloadInfo info) {
		try {
			Dao<DownloadInfo, String> dao = mDBhelper
					.getDao(DownloadInfo.class);
			if (info.getStatus() != DownloadStatus.DELETE) {
				dao.createOrUpdate(info);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<DownloadInfo> query(int userId) {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			return dao.query(dao.queryBuilder().where().eq("uid", userId)
					.prepare());
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public List<DownloadInfo> query(int uid, String moduleId) {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			return dao.query(dao.queryBuilder().where().eq("uid", uid).and()
					.eq("parentId", moduleId).prepare());
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public DownloadInfo query(int uid, String moduleId, String videoId) {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			List<DownloadInfo> list = dao.query(dao.queryBuilder().where()
					.eq("uid", uid).and().eq("parentId", moduleId).and()
					.eq("vid", videoId).prepare());
			if (list != null && list.size() > 0) {
				return list.get(0);
			}
			return null;
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public List<DownloadInfo> queryAll() {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			return (ArrayList<DownloadInfo>) dao.query(dao.queryBuilder()
					.prepare());
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public List<DownloadInfo> queryUnfinished() {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			return (ArrayList<DownloadInfo>) dao.query(dao.queryBuilder()
					.where().ne("status", DownloadStatus.DONE).prepare());
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public List<DownloadInfo> queryDownloading() {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			return (ArrayList<DownloadInfo>) dao
					.query(dao.queryBuilder().where()
							.eq("status", DownloadStatus.DOWNLOADING).prepare());
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public List<DownloadInfo> queryWaiting() {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			return (ArrayList<DownloadInfo>) dao.query(dao.queryBuilder()
					.where().eq("status", DownloadStatus.WAITING).prepare());
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public List<DownloadInfo> queryFinish() {
		Dao<DownloadInfo, String> dao;
		try {
			dao = mDBhelper.getDao(DownloadInfo.class);
			return (ArrayList<DownloadInfo>) dao.query(dao.queryBuilder()
					.where().eq("status", DownloadStatus.DONE).prepare());
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public DownloadInfo queryById(String id) {
		try {
			Dao<DownloadInfo, String> dao = mDBhelper
					.getDao(DownloadInfo.class);
			return dao.queryForId(id);
		} catch (SQLException e) {
			MyLog.d(e.getMessage());
			return null;
		}
	}

	public int deleteById(String id) {
		try {
			Dao<DownloadInfo, String> dao = mDBhelper
					.getDao(DownloadInfo.class);
			return dao.deleteById(id);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return -1;
	}

	public int deleteAll() {
		try {
			Dao<DownloadInfo, String> dao = mDBhelper
					.getDao(DownloadInfo.class);
			return dao.executeRawNoArgs(SQL_DELETE_ALL);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return -1;
	}

}
