package cn.woblog.android.downloader.utils;

public class Consts {

	public static final String KEY_DATA = "key_data";
	public static final String KEY_ACTION = "key_action";
	public static final int MAX_TASK_COUNT = 2;
	public static final int CONNECT_TIMEOUT = 10000;
	public static final int READ_TIMEOUT = 30000;
	public static final int BUFFER_SIZE = 4096;
	public static final int MSG_CHECK_NEXT = 0;
	public static final int MSG_DOWNLOAD_COMPLETE = 1;

}
