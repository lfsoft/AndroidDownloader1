package cn.woblog.android.downloader.utils;

import android.util.Log;

public class MyLog {
	private static final String TAG = "ad";
	private static boolean isDebug = true;

	public static void d(String msg) {
		if (isDebug) {
			Log.d(TAG, msg);
		}
	}

	public static void d(String tag, String msg) {
		if (isDebug) {
			Log.d(TAG, msg);
		}
	}
}
