package cn.woblog.android.downloader.thread;

import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import android.content.Context;
import cn.woblog.android.downloader.domain.DownloadInfo;
import cn.woblog.android.downloader.domain.DownloadStatus;
import cn.woblog.android.downloader.listener.DownloadListener;
import cn.woblog.android.downloader.utils.Consts;
import cn.woblog.android.downloader.utils.FileUtils;
import cn.woblog.android.downloader.utils.MyLog;

public class DownloadThread extends Thread {

	private Context context;
	private DownloadInfo info;
	private DownloadListener listener;
	private boolean isStop;
	private DownloadStatus status;
	private DownloadTask task;
	private long currentProgress;

	public DownloadThread(DownloadTask task, Context context,
			DownloadInfo info, DownloadListener listener) {
		this.task = task;
		this.context = context;
		this.info = info;
		this.listener = listener;
		this.currentProgress = info.getProgress();
	}

	@SuppressWarnings("resource")
	@Override
	public void run() {
		try {
			HttpURLConnection httpUrlConnection = FileUtils
					.getHttpUrlConnection(true, info.getUrl(),
							info.getProgress(), info.getSize());
			RandomAccessFile raf = new RandomAccessFile(info.getSavePath(),
					"rw");
			raf.seek(info.getProgress());
			int len = 0;
			int total = 0;
			byte[] buffer = new byte[Consts.BUFFER_SIZE];
			InputStream is = httpUrlConnection.getInputStream();
			while (!isStop && (len = is.read(buffer)) != -1) {
				raf.write(buffer, 0, len);
				total += len;
				MyLog.d("downloading threadId:" + getId() + ":" + info.getId()
						+ ":" + info.getProgress() + ":" + info.getSize());
				if (listener != null) {
					info.setProgress(currentProgress + total);
					listener.onDownloadProgressChanged(total, info);
				}
				if (status == DownloadStatus.PAUSE) {
					synchronized (task) {
						try {
							MyLog.d("pause " + info.getId());
							if (listener != null) {
								listener.onDownloadStatusChanged(info);
							}
							task.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
			is.close();
			raf.close();
			if (status != DownloadStatus.DELETE) {
				MyLog.d("download status changed " + info.getId() + ":"
						+ info.getStatus());
				if (listener != null) {
					listener.onDownloadComplete(info);
				}
			} else {
				MyLog.d("delete " + info.getId() + " success");
				if (listener != null) {
					listener.onDownloadStatusChanged(info);
				}
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (ProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		super.run();
	}

	public void setStatus(DownloadStatus status) {
		this.status = status;
	}

	public void setStop(boolean isStop) {
		MyLog.d("setStop");
		this.isStop = isStop;
		status = DownloadStatus.DELETE;
		info.setStatus(DownloadStatus.DELETE);
	}

	public void pause(boolean b) {
		if (b) {
			status = DownloadStatus.PAUSE;
			info.setStatus(DownloadStatus.PAUSE);
		} else {
			status = DownloadStatus.DOWNLOADING;
			info.setStatus(DownloadStatus.DOWNLOADING);
		}

	}
}
