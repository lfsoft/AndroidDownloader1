package cn.woblog.android.downloader.test;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import cn.woblog.android.downloader.DownloadManager;
import cn.woblog.android.downloader.R;
import cn.woblog.android.downloader.db.DBController;
import cn.woblog.android.downloader.domain.DownloadInfo;
import cn.woblog.android.downloader.domain.DownloadStatus;
import cn.woblog.android.downloader.observable.DataWatcher;
import cn.woblog.android.downloader.service.DownloadService;
import cn.woblog.android.downloader.test.DownloadAdapter.OnDownloadItemClickListener;
import cn.woblog.android.downloader.test.domain.AppData;
import cn.woblog.android.downloader.test.view.AppDataItem;
import cn.woblog.android.downloader.utils.MyLog;

public class MainActivity extends FragmentActivity implements
		OnCheckedChangeListener, OnDownloadItemClickListener {

	private static final String TAG = "MainActivity";
	private DownloadManager downloadManager;
	private DataWatcher watcher = new DataWatcher() {
		protected void onDataChanged(DownloadInfo data) {
			// tv_resutl.setText(data.getUrl() + ":" + data.getProgress() + ":"
			// + data.getSize() + ":" + data.getStatus());
			updateItem(data);
		};

		protected void onDataDeleted(DownloadInfo data) {

			// tv_resutl.setText("删除：" + data.getUrl());
			deleteItem(data);

		};
	};
	private RadioGroup rg;
	private ListView lv_downloading;
	private ListView lv_downloaded;
	private List<AppData> appDatas = new ArrayList<AppData>();
	private List<AppData> downloadedDatas = new ArrayList<AppData>();
	private DownloadAdapter downloadingAdapter;
	private DownloadAdapter downloadedAdapter;
	private DBController dbController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		downloadManager = DownloadManager.getInstance(getApplicationContext());
		initViews();
		initTestDatas();
		initDatas();
		initListener();
	}

	protected void deleteItem(DownloadInfo data) {
		if (appDatas != null && appDatas.size() > 0) {
			int removeIndex = -1;
			for (int i = 0; i < appDatas.size(); i++) {
				AppData appData = appDatas.get(i);
				if (appData.getUrl().equalsIgnoreCase(data.getId())) {
					removeIndex = i;
					break;
				}
			}
			if (removeIndex != -1) {
				appDatas.remove(removeIndex);
				downloadingAdapter.notifyDataSetChanged();
			}
		}
	}

	protected void updateItem(DownloadInfo data) {
		MyLog.d(TAG, data.getId() + ":" + data.getStatus());
		if (data.getStatus() == DownloadStatus.DONE) {
			AppData removeData = removeData(data);
			if (removeData != null) {
				addData2Downloaded(removeData);
			}
		} else {
			newOrUpadte(data);
			View view = lv_downloading.findViewWithTag(data.getUrl());
			if (view != null) {
				AppDataItem item = (AppDataItem) view;
				item.setdata(data);
			}
		}

	}

	private void addData2Downloaded(AppData data) {
		downloadedDatas.add(data);
		downloadedAdapter.notifyDataSetChanged();
	}

	private AppData removeData(DownloadInfo data) {
		AppData removeIndex = null;
		for (AppData item : appDatas) {
			if (item.getUrl().equalsIgnoreCase(data.getId())) {
				removeIndex = item;
			}
		}
		if (removeIndex != null) {
			appDatas.remove(removeIndex);
			downloadingAdapter.notifyDataSetChanged();
		}
		return removeIndex;
	}

	private void newOrUpadte(DownloadInfo data) {
		for (AppData item : appDatas) {
			if (item.getUrl().equalsIgnoreCase(data.getId())) {
				item.setStatus(data.getStatus());
			}
		}
	}

	private void initTestDatas() {
		AppData appData = new AppData(
				"baidu.apk",
				"http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/6/5c/824943b65799bc95dfad6b6ad0a565c6.apk");
		appDatas.add(appData);
		appData = new AppData(
				"sougoumap.apk",
				"http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/e/9c/51ca1a99f9d5c83faf246cae236539ce.apk");
		appDatas.add(appData);
		appData = new AppData(
				"qq.apk",
				"http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/5/9a/7f3560c20899d41e65dbd6815a1929a5.apk");
		appDatas.add(appData);
		appData = new AppData(
				"evernote.apk",
				"http://shouji.360tpcdn.com/141127/a6ae91ec8a3fcfc72809ccebbd0f941d/com.evernote_1062123.apk");
		appDatas.add(appData);
		appData = new AppData(
				"meitu.apk",
				"http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/2/13/ecc35bf0e0a911dc50371538cd7c8132.apk");
		appDatas.add(appData);
		appData = new AppData(
				"camera360.apk",
				"http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/d/37/c2cfebf9cd89311590078fcdfd35637d.apk");
		appDatas.add(appData);

		// appDatas.add(appData);
		// appData = new AppData(
		// "sougoumap.apk",
		// "http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/e/9c/51ca1a99f9d5c83faf246cae236539ce.apk");
		// appDatas.add(appData);
		// appData = new AppData(
		// "qq.apk",
		// "http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/5/9a/7f3560c20899d41e65dbd6815a1929a5.apk");
		// appDatas.add(appData);
		// appData = new AppData(
		// "qq.apk",
		// "http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/5/9a/7f3560c20899d41e65dbd6815a1929a5.apk");
		// appDatas.add(appData);
		// appData = new AppData(
		// "evernote.apk",
		// "http://shouji.360tpcdn.com/141127/a6ae91ec8a3fcfc72809ccebbd0f941d/com.evernote_1062123.apk");
		// appDatas.add(appData);
		// appData = new AppData(
		// "meitu.apk",
		// "http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/2/13/ecc35bf0e0a911dc50371538cd7c8132.apk");
		// appDatas.add(appData);
		// appData = new AppData(
		// "camera360.apk",
		// "http://111.206.14.153/m.wdjcdn.com/apk.wdjcdn.com/d/37/c2cfebf9cd89311590078fcdfd35637d.apk");
		// appDatas.add(appData);

		downloadingAdapter = new DownloadAdapter(this, appDatas, 0, this);
		lv_downloading.setAdapter(downloadingAdapter);
	}

	private void initListener() {
		rg.setOnCheckedChangeListener(this);
	}

	private void initDatas() {
		dbController = DBController.getInstance(getApplicationContext());
		downloadedAdapter = new DownloadAdapter(this, downloadedDatas, 1, this);
		lv_downloaded.setAdapter(downloadedAdapter);
	}

	private void initViews() {
		rg = (RadioGroup) findViewById(R.id.rg);
		rg.check(R.id.rb_downloading);
		lv_downloading = (ListView) findViewById(R.id.lv_downloading);
		lv_downloaded = (ListView) findViewById(R.id.lv_downloaded);
	}

	@Override
	protected void onResume() {
		downloadManager.addObserver(watcher);
		super.onResume();
	}

	@Override
	protected void onPause() {
		downloadManager.removeObserver(watcher);
		super.onPause();
	}

	public void initTestDatas(View view) {
		stopService(new Intent(getApplicationContext(), DownloadService.class));
		dbController.deleteAll();
		finish();
	}

	// public void a(View view) {
	// DownloadInfo info = new DownloadInfo();
	// info.setUrl("http://bcs.duapp.com/testdownloaddata/baidu.apk");
	// info.setSavePath(Environment.getExternalStorageDirectory()
	// .getAbsolutePath() + "/baidu1.apk");
	// downloadManager.add(info);
	// }
	//
	// public void pa(View view) {
	// downloadManager
	// .pause("http://bcs.duapp.com/testdownloaddata/baidu.apk");
	// }
	//
	// public void ra(View view) {
	// downloadManager
	// .resume("http://bcs.duapp.com/testdownloaddata/baidu.apk");
	// }
	//
	// public void da(View view) {
	// downloadManager
	// .delete("http://bcs.duapp.com/testdownloaddata/baidu.apk");
	// }

	@Override
	public void onCheckedChanged(RadioGroup group, int checkedId) {
		switch (checkedId) {
		case R.id.rb_downloading:
			lv_downloading.setVisibility(View.VISIBLE);
			lv_downloaded.setVisibility(View.GONE);
			break;
		case R.id.rb_downloaded:
			lv_downloading.setVisibility(View.GONE);
			lv_downloaded.setVisibility(View.VISIBLE);
			break;

		default:
			break;
		}
	}

	@Override
	public void OnDownloadItemClick(int position, int action, int type) {
		switch (action) {
		case 2:
			deleteControl(position, type);
			break;
		case -1:
			downloadControl(position, type);
			break;

		default:

			break;
		}
	}

	private void downloadControl(int position, int type) {
		if (type == 0) {
			AppData appData = appDatas.get(position);
			DownloadStatus status = appData.getStatus();
			switch (status) {
			case UNDOWNLOAD:
				DownloadInfo info = new DownloadInfo(appData.getUrl(),
						appData.getSavePath());
				downloadManager.add(info);
				break;
			case DOWNLOADING:

				downloadManager.pause(appData.getUrl());
				break;
			case PAUSE:
				downloadManager.resume(appData.getUrl());
				break;

			default:
				break;
			}
		}
	}

	private void deleteControl(int position, int type) {
		AppData appData = appDatas.get(position);
		if (appData != null) {
			downloadManager.delete(appData.getUrl());
		}
	}

}
