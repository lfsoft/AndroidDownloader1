package cn.woblog.android.downloader.test.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import cn.woblog.android.downloader.R;
import cn.woblog.android.downloader.domain.DownloadInfo;
import cn.woblog.android.downloader.domain.DownloadStatus;
import cn.woblog.android.downloader.test.DownloadAdapter.OnDownloadItemClickListener;
import cn.woblog.android.downloader.test.domain.AppData;

@SuppressLint("NewApi")
public class AppDataItem extends LinearLayout implements OnClickListener {

	private TextView tv_url;
	private TextView tv_info;
	private Button bt_control;
	private Button bt_delete;
	private OnDownloadItemClickListener listener;
	private int type;

	public AppDataItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		init();
	}

	private void init() {
		initViews();
		setListeners();
	}

	private void setListeners() {
		bt_control.setOnClickListener(this);
		bt_delete.setOnClickListener(this);
	}

	private void initViews() {
		View view = View.inflate(getContext(), R.layout.item_download, null);
		tv_url = (TextView) view.findViewById(R.id.tv_url);
		tv_info = (TextView) view.findViewById(R.id.tv_info);
		bt_control = (Button) view.findViewById(R.id.bt_control);
		bt_delete = (Button) view.findViewById(R.id.bt_delete);
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
		addView(view, layoutParams);
	}

	public AppDataItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	public AppDataItem(Context context) {
		super(context);
		init();
	}

	public void setOnDownloadItemClickListener(
			OnDownloadItemClickListener listener) {
		this.listener = listener;
	}

	public void setData(AppData info, int position, int type) {
		tv_url.setText(info.getUrl());
		tv_info.setVisibility(GONE);
		if (info.getStatus() == DownloadStatus.DONE) {
			bt_control.setVisibility(GONE);
			bt_delete.setVisibility(VISIBLE);
		} else {
			bt_control.setText("download");
			bt_delete.setVisibility(GONE);
		}
		this.type = type;
		bt_control.setTag(position);
	}

	@Override
	public void onClick(View v) {
		if (listener != null) {
			switch (v.getId()) {
			case R.id.bt_control:
				listener.OnDownloadItemClick((Integer) bt_control.getTag(), -1,
						type);
				break;
			case R.id.bt_delete:
				listener.OnDownloadItemClick((Integer) bt_control.getTag(), 2,
						type);
				break;

			default:
				break;
			}
		}
	}

	public void setdata(DownloadInfo info) {
		bt_delete.setVisibility(VISIBLE);
		tv_info.setVisibility(VISIBLE);
		switch (info.getStatus()) {
		case DOWNLOADING:
			bt_control.setVisibility(View.VISIBLE);
			bt_control.setText("pause");
			tv_info.setText(info.getProgress() + ":" + info.getSize());
			break;
		case PAUSE:
			bt_control.setVisibility(View.VISIBLE);
			bt_control.setText("resume");
			break;
		case DONE:
			bt_control.setVisibility(View.GONE);
			break;
		case WAITING:
			bt_control.setText("wating");
			break;

		default:
			break;
		}
	}
}
