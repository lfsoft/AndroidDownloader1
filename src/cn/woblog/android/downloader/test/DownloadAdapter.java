package cn.woblog.android.downloader.test;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import cn.woblog.android.downloader.R;
import cn.woblog.android.downloader.test.domain.AppData;
import cn.woblog.android.downloader.test.view.AppDataItem;

public class DownloadAdapter extends BaseAdapter {
	private Context context;
	private List<AppData> datas;
	private int type;
	private OnDownloadItemClickListener listener;
	private AppData info;

	public DownloadAdapter(Context context, List<AppData> datas, int type,
			OnDownloadItemClickListener listener) {
		this.context = context;
		this.datas = datas;
		this.type = type;
		this.listener = listener;
	}

	@Override
	public int getCount() {

		return datas.size();
	}

	@Override
	public Object getItem(int position) {

		return datas.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {

		AppDataItem appDataItem = null;
		if (convertView == null) {
			appDataItem = new AppDataItem(context);
		} else {
			appDataItem = (AppDataItem) convertView;
		}

		info = datas.get(position);
		appDataItem.setData(info, position, type);
		appDataItem.setOnDownloadItemClickListener(listener);
		appDataItem.setTag(info.getUrl());
		// tv_info.setText(info.getProgress() + "/" + info.getSize());
		// switch (info.getStatus()) {
		// case DOWNLOADING:
		// bt_control.setVisibility(View.VISIBLE);
		// bt_control.setText("pause");
		// bt_control.setTag(0);
		// break;
		// case PAUSE:
		// bt_control.setVisibility(View.VISIBLE);
		// bt_control.setText("resume");
		// bt_control.setTag(1);
		// break;
		// case DONE:
		// bt_control.setVisibility(View.GONE);
		// break;
		//
		// default:
		// break;
		// }

		return appDataItem;
	}

	public interface OnDownloadItemClickListener {
		/**
		 * 
		 * @param position
		 *            被点击item位置
		 * @param action
		 *            动作,2 删除,-1 根据当前集合下载状态作出变更
		 * @param type
		 *            是那个集合的被点击了 0 正在下载列表，1 下载完成列表
		 */
		void OnDownloadItemClick(int position, int action, int type);

		// void OnDownloadItemClick(int position, int action);
	}
}
