package cn.woblog.android.downloader.test.domain;

import cn.woblog.android.downloader.domain.DownloadInfo;
import cn.woblog.android.downloader.domain.DownloadStatus;
import cn.woblog.android.downloader.utils.FileUtils;

public class AppData extends DownloadInfo {
	/**
	 * 
	 */
	private static final long serialVersionUID = -8993515398610020650L;
	private String name;
	private String url;

	public AppData(String name, String url) {
		super();
		this.name = name;
		this.url = url;
		setStatus(DownloadStatus.UNDOWNLOAD);
		setSavePath(FileUtils.getDownloadRootPath(name));
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

}
