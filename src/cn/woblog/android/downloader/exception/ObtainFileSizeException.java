package cn.woblog.android.downloader.exception;

public class ObtainFileSizeException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8497516498794209792L;

	public ObtainFileSizeException() {
		super();

	}

	public ObtainFileSizeException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);

	}

	public ObtainFileSizeException(String detailMessage) {
		super(detailMessage);

	}

	public ObtainFileSizeException(Throwable throwable) {
		super(throwable);

	}

}
