package cn.woblog.android.downloader.observable;

import java.util.Observable;
import java.util.Observer;

import cn.woblog.android.downloader.domain.DownloadInfo;
import cn.woblog.android.downloader.domain.DownloadStatus;

public class DataWatcher implements Observer {

	@Override
	public void update(Observable observable, Object data) {
		if (data != null) {
			if (data instanceof DownloadInfo) {
				DownloadInfo data1 = (DownloadInfo) data;
				if (data1.getStatus() == DownloadStatus.DELETE) {
					onDataDeleted(data1);
				} else {
					onDataChanged(data1);
				}
			}
		}
	}

	protected void onDataChanged(DownloadInfo data) {

	}

	protected void onDataDeleted(DownloadInfo data) {

	}

}
