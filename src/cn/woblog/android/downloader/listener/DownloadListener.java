package cn.woblog.android.downloader.listener;

import cn.woblog.android.downloader.domain.DownloadInfo;

public interface DownloadListener {
	void onDownloadProgressChanged(long l, DownloadInfo data);

	void onDownloadStatusChanged(DownloadInfo data);

	void onDownloadComplete(DownloadInfo data);

}
