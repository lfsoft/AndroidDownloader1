package cn.woblog.android.downloader.domain;

import java.io.Serializable;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "DownloadInfo")
public class DownloadInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 341090936150862507L;
	@DatabaseField(id = true)
	private String id;
	@DatabaseField
	private String url;
	@DatabaseField
	private String name;
	@DatabaseField
	private String savePath;
	@DatabaseField
	private long progress;
	@DatabaseField
	private long size;
	@DatabaseField
	private DownloadStatus status;

	public DownloadInfo() {
	}

	public DownloadInfo(String url) {
		this.url = url;
		setId(url);
	}

	public DownloadInfo(String url, String savePath) {
		this.url = url;
		this.savePath = savePath;
		setId(url);
	}

	public String getSavePath() {
		return savePath;
	}

	public void setSavePath(String savePath) {
		this.savePath = savePath;
	}

	public DownloadStatus getStatus() {
		return status;
	}

	public void setStatus(DownloadStatus status) {
		this.status = status;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
		setId(url);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getProgress() {
		return progress;
	}

	public void setProgress(long progress) {
		this.progress = progress;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

}
