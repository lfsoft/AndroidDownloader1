package cn.woblog.android.downloader.domain;

public enum DownloadStatus {
	ADD, DOWNLOADING, DONE, PAUSE, RESUME, DELETE, GET_FILE_SIZE_ERROR, GET_SIZEING,WAITING, UNDOWNLOAD
}
